# DeliveryTest#

### What is this repository for? ###

* This repository is meant to host the code for a test application that will run in the server side of the platform.
* 0.1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone the project and make sure your jdk/jre is 1.8 or higher.
* Let gradle download all necesary dependencies you dont already have.
* Download MySQL v5 or higher.
* Create and empty schema and call it "deliverytest".
* Run using bootRun and let the program create the tables and associations in the database.
* Tests are not yet implemented. Feel free to do so. 

### Contribution guidelines ###

* Writing tests: anyone can contribute to that.
* Code review: Francisco Farías.
* Other guidelines

### Who do I talk to? ###

* Repo owner: Francisco Farías